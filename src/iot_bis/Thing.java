package iot_bis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Thing {
	private String userId;
	private String macAdress;
	private Map<String, String> mapData;
	private ArrayList<Service> arrServices;

	public Thing(String userId, String macAdress) {
		this.userId = userId;
		this.macAdress = macAdress;
		this.arrServices = new ArrayList<Service>();
		this.mapData = new HashMap<String, String>();
	}

	public String getMacAdress() {
		return macAdress;
	}

	public String getUserId() {
		return userId;
	}

	public void putData(String key, String dat) {
		mapData.put(key, dat);
	}

	public String getData(String key) {
		return mapData.get(key);
	}

	public void setFromDatagram(String datagram) {
		String[] arrdata = datagram.split(";");
		for (String a : arrdata) {
			String[] arrkey = a.split(" ");
			mapData.put(arrkey[0], arrkey[1]);
		}
	}

	public Boolean existData(String key) {
		System.out.println("existedata");
		System.out.println(key+""+mapData.containsKey(key));
		return mapData.containsKey(key);
	}

	public void resetData() {
		mapData.clear();
	}

	public void subscribe(Service service) {
		arrServices.add(service);
	}

	public void update() {
		 for (int i=0 ; i < this.arrServices.size() ; i++) {
		        this.arrServices.get(i).writedata(this);
		    }
	}

	@Override
	public String toString() {
		StringBuffer ret =new StringBuffer();
		ret.append(macAdress+";"+userId+";");
		Iterator<Map.Entry<String, String>> it = this.mapData.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry<String, String> couple = it.next();
		    String dat = (String)couple.getValue() ;
		    ret.append(dat+";");
		}

		String retfinal=new String(ret);
		return retfinal;
		
	}
	
	
}
