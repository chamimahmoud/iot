package iot_bis;

import java.util.HashMap;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import com.mysql.jdbc.Connection;

public class Platform {
	private Map<String, Thing> mapThings;
	private ArrayList<Service> arrServices;

	public Platform() {
		this.mapThings = new HashMap<String, Thing>();
		this.arrServices = new ArrayList<Service>();
	}

	// thing
	public void addThing(Thing thing) {
		mapThings.put(thing.getMacAdress(), thing);
	}

	public void addService(Service service) {
		arrServices.add(service);
	}

	public void run(DataReceiver dataReceiver) throws IOException {
		String datagram;

		while (dataReceiver.ready()) {

			datagram = dataReceiver.readDatagram();
			if (datagram != null && !datagram.isEmpty()) {
				String mac = datagram.substring(0, 17);
				System.out.println(mac);
				Thing theThing = this.mapThings.get(mac);
				if (theThing == null) {
					System.out.println("Mac address unknown: " + mac);
				} else {
					theThing.setFromDatagram(datagram.substring(18));
					theThing.update();
					theThing.resetData();
				}
			}
		}
	}

	public void close() {

		for (int i = 0; i < this.arrServices.size(); i++) {
			// Service service = this.arrServices.get(i);
			// service.close();
			arrServices.get(i).close();
		}

	}

	public void loadFromDatabase() throws SQLException, IOException {
		Service s11;
		Map<String, Service> MapIds = new HashMap<String, Service>();
		ConnexionBDDModele conn1 = new ConnexionBDDModele();
		java.sql.Connection conn = conn1.getConnexion();
		String query = "SELECT * FROM table_service";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			String id = rs.getString("id");
			String name = rs.getString("name");
			String type = rs.getString("type");
			if (type.equals("smarthome")) {
				s11 = new SmartHome(name);
				addService(s11);
			} else {
				if (type.equals("quantifiedself")) {
					s11 = new QuantifiedSelf(name);
					addService(s11);
				} else {

					s11 = new Service(name);
					addService(s11);
				}
			}
			MapIds.put(id, s11);
		}
		rs.close();

		// ##############################quest2###################
		Thing thing;
		String query2 = "SELECT * FROM table_thing";
		ResultSet rs2 = st.executeQuery(query2);
		Statement st1 = conn.createStatement();
		while (rs2.next()) {
			String idUser = rs2.getString("id_user");
			System.out.println(idUser);
			System.out.println(idUser);
			String typeThing = rs2.getString("type");
			String parameterThing = rs2.getString("param");
			String macThing = rs2.getString("mac");
			System.out.println(typeThing);
			if (typeThing.equals("thingtempo")) {
				System.out.println(parameterThing);
				thing = new ThingTempo(macThing, idUser, Long.parseLong(parameterThing));
				System.out.println(thing);
			} else {
				thing = new Thing(idUser, macThing);
				System.out.println(thing);
			}
			this.addThing(thing);
			System.out.println(macThing);
			String query3 = "SELECT id_service FROM table_subscribe WHERE id_user IN (SELECT id_user FROM table_thing WHERE mac='"
					+ macThing + "');";
			ResultSet rs3 = st1.executeQuery(query3);
			String idService = null;
			while (rs3.next())
				idService = rs3.getString("id_service");
			System.out.println(idService);
			Service serviceTosub = MapIds.get(idService);
			thing.subscribe(serviceTosub);
		}
		rs2.close();

	}
}
