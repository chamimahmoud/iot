package iot_bis;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileReader implements DataReceiver {
	private String filename;
	private BufferedReader br;
	private boolean ok;

	public FileReader(String filename) {

		this.filename = filename;

	}

	public void open() {
		InputStream ips;
		try {
			ips = new FileInputStream(this.filename);
			InputStreamReader ipsr = new InputStreamReader(ips);
			br = new BufferedReader(ipsr);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ok = true;

	}

	public String readDatagram() throws IOException {
		String ret = br.readLine();
			
			if (ret == null || ret.isEmpty()) {
				System.out.println("ret null");
				ok = false;
				return null;
			}
			System.out.println(ret);
			return ret;
	}

	public boolean ready() {
//		System.out.println(ok);
		return ok;
	}

	public void close() {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ok = false;

	}
}
