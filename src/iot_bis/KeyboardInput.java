package iot_bis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KeyboardInput implements DataReceiver {
	private boolean ok;

	public KeyboardInput() {

		ok = false;
	}


	@Override
	public void open() {
		// TODO Auto-generated method stub
		ok = true;
	}

	@Override
	public String readDatagram() {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String mac = null,datagram = null;
		try {
			System.out.print("MAC ADDRESS: ");
			 mac=br.readLine();
			 if(mac.equals("quit")) {
				 ok=false;
				 return null;
			 }
			System.out.print("DARAGRAM: ");
			 datagram=br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mac+";"+datagram;
	}

	@Override
	public boolean ready() {
		// TODO Auto-generated method stub
		return ok;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		ok = false;
	}
}
