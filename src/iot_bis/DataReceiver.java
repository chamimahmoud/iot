package iot_bis;

import java.io.IOException;

public interface DataReceiver {

	public void open();
	public String readDatagram() throws IOException; 
	public boolean ready();
	public void close();

}
