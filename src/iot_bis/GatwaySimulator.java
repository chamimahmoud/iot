package iot_bis;

import java.io.IOException;

public class GatwaySimulator {
	void exo1() throws IOException {
//		Instanciez un objet f de classe FileReader qui lira depuis le fichier "simu.txt".
		FileReader f= new FileReader( "simu.txt");
//		Instanciez un objet s de classe SocketClient qui se connectera � la machine d'adresse IP "127.0.0.1" et �crira sur le port 51291.
		SocketClient s=new SocketClient("127.0.0.1",51291);
		f.open();
		s.open();
//		Tant que le FileReader f est pr�t � lire
		while(f.ready()) {
//			Lire un datagramme depuis le fichier avec le FileReader f
			String test=f.readDatagram();
//			Si le datagramme n'est pas null
			if(test!=null) {
//				alors envoyer le datagramme sur le r�seau gr�ce au SocketClient s
				s.writeDatagram(test);
			}
		}
//		Appelez la m�thode close() de ces deux objets.
		f.close();
		s.close();
		
	}
	public GatwaySimulator() throws IOException{
		exo1();
	}
public static void main(String[] args) throws IOException {
	new GatwaySimulator();
}
}
