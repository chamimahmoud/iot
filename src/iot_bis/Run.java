package iot_bis;

import java.io.IOException;
import java.sql.SQLException;

public class Run {
	void exo1_Q1_2() throws IOException {
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		System.out.println("Welcome on IoT central platform.");
		t1.setFromDatagram("geo 43.433331 -1.58333;pul 128;bat 90.0");
		System.out.println(t1.toString());
		System.out.println("bye.");
	}

	void exo1_Q1_3() throws IOException {
		Service s1 = new Service("mon_service");
		System.out.println("Welcome on IoT central platform.");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		s1.close();
		System.out.println("bye.");
	}

	void exo1_Q1_4() throws IOException {
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		s1.close();
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		// Appelez la méthode update() de t1.
		t1.update();
		System.out.println("bye.");

	}

	void exo1_Q1_5() throws IOException {
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		s1.close();
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		// Appelez la méthode update() de t1.
		t1.update();
		// Instanciez un objet k de classe KeyboardInput.
		KeyboardInput k = new KeyboardInput();
		// Invoquez sa méthode open().
		k.open();
		String result=k.readDatagram();
		if (result != null)
			System.out.println("result: " + result);
		k.close();
		System.out.println("bye.");

	}
	void exo1_Q1_6() throws IOException {
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		// Appelez la méthode update() de t1.
		t1.update();
		// Instanciez un objet k de classe KeyboardInput.
		KeyboardInput k = new KeyboardInput();
		// Invoquez sa méthode open().
		k.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
		p.addService(s1);
		p.addService(s2);
		p.addService(s3);
		p.addService(s4);
		p.addService(s5);
		p.addThing(t1);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
		p.run(k);
		k.close();
		s1.close();
		s2.close();
		s3.close();
		s4.close();
		s5.close();
		System.out.println("bye.");
	}
	void exo2() throws IOException{
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		// Appelez la méthode update() de t1.
		t1.update();
		// Instanciez un objet k de classe KeyboardInput.
//		KeyboardInput k = new KeyboardInput();
//		Créez un nouvel objet f de classe FileReader et utilisez-le pour remplacer le KeyboardInput utilisé précédemment
		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
//		k.open();
		f.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
		p.addService(s1);
		p.addService(s2);
		p.addService(s3);
		p.addService(s4);
		p.addService(s5);
		p.addThing(t1);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		p.run(k);
		p.run(f);
//		k.close();
		f.close();
		s1.close();
		s2.close();
		s3.close();
		s4.close();
		s5.close();
		System.out.println("bye.");
	}
	void exo3_1() throws IOException{
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
//		Créez un nouveau service sh de classe SmartHome portant le nom « myKWHome »
		Service sh = new SmartHome("myKWHome");
		
		
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		t1.subscribe(sh);
		// Appelez la méthode update() de t1.
		t1.update();
		// Instanciez un objet k de classe KeyboardInput.
		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
//		k.open();
		f.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
		p.addService(s1);
		p.addService(s2);
		p.addService(s3);
		p.addService(s4);
		p.addService(s5);
//		Ajoutez-le à la plateforme p.
		p.addService(sh);
		p.addThing(t1);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		p.run(k);
		p.run(f);
//		k.close();
		s1.close();
		s2.close();
		s3.close();
		s4.close();
		s5.close();
		sh.close();
		System.out.println("bye.");
	}
	void exo3_2() throws IOException{
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
		s1.writedata(t1);
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
//		Créez un nouveau service sh de classe SmartHome portant le nom « myKWHome »
		Service sh = new SmartHome("myKWHome");
//		Créez un nouveau service qs de classe QuantifiedSelf portant le nom « RUNstats »
		Service qs = new QuantifiedSelf("RUNstats");
		
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		t1.subscribe(sh);
		t1.subscribe(qs);
		
		// Appelez la méthode update() de t1.
		t1.update();
		// Instanciez un objet k de classe KeyboardInput.
		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
//		k.open();
		f.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
		p.addService(s1);
		p.addService(s2);
		p.addService(s3);
		p.addService(s4);
		p.addService(s5);
//		Ajoutez sh à la plateforme p.
		p.addService(sh);
//		Ajoutez-qs à la plateforme p
		p.addService(qs);
		p.addThing(t1);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		p.run(k);
		p.run(f);
//		k.close();
		s1.close();
		s2.close();
		s3.close();
		s4.close();
		s5.close();
		sh.close();
		qs.close();
		System.out.println("bye.");
	}
	void exo3_3() throws IOException{
		System.out.println("Welcome on IoT central platform.");
		Service s1 = new Service("mon_service");
		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
//		Instanciez un objet t2 de classe ThingTempo avec comme adresse MAC "f0:de:f1:39:7f:18", 1 comme id utilisateur et 60 comme délai en secondes.
		Thing t2 = new ThingTempo("f0:de:f1:39:7f:18", "1",60);
		s1.writedata(t1);
		
		// Créez quatre nouveaux services s2, s3, s4 et s5.
		Service s2 = new Service("mon_service2");
		Service s3 = new Service("mon_service3");
		Service s4 = new Service("mon_service4");
		Service s5 = new Service("mon_service5");
//		Créez un nouveau service sh de classe SmartHome portant le nom « myKWHome »
		Service sh = new SmartHome("myKWHome");
//		Créez un nouveau service qs de classe QuantifiedSelf portant le nom « RUNstats »
		Service qs = new QuantifiedSelf("RUNstats");
		
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
		t1.subscribe(s1);
		t1.subscribe(s2);
		t1.subscribe(s3);
		t1.subscribe(s4);
		t1.subscribe(s5);
		t1.subscribe(sh);
		t1.subscribe(qs);
//		Abonnez-le à au moins deux services de s1 à s5 (hors QuantifiedSelf et SmartHome).
		t2.subscribe(s1);
		t2.subscribe(s2);
		t2.subscribe(s3);
		t2.subscribe(s4);
		t2.subscribe(s5);
		// Appelez la méthode update() de t1.
		t1.update();
//		 Instanciez un objet k de classe KeyboardInput.
		KeyboardInput k=new KeyboardInput();
//		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
		k.open();
//		f.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
		p.addService(s1);
		p.addService(s2);
		p.addService(s3);
		p.addService(s4);
		p.addService(s5);
//		Ajoutez sh à la plateforme p.
		p.addService(sh);
//		Ajoutez-qs à la plateforme p
		p.addService(qs);
		p.addThing(t1);
//		Ajoutez-le à la plateforme p.
		p.addThing(t2);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		p.run(k);
		p.run(k);
		k.close();
		s1.close();
		s2.close();
		s3.close();
		s4.close();
		s5.close();
		sh.close();
		qs.close();
		System.out.println("bye.");
	}
	void exo4() throws IOException, SQLException{
		System.out.println("Welcome on IoT central platform.");
//		Service s1 = new Service("mon_service");
//		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
//		Instanciez un objet t2 de classe ThingTempo avec comme adresse MAC "f0:de:f1:39:7f:18", 1 comme id utilisateur et 60 comme délai en secondes.
//		Thing t2 = new ThingTempo("f0:de:f1:39:7f:18", "1",60);
//		s1.writedata(t1);
		
		// Créez quatre nouveaux services s2, s3, s4 et s5.
//		Service s2 = new Service("mon_service2");
//		Service s3 = new Service("mon_service3");
//		Service s4 = new Service("mon_service4");
//		Service s5 = new Service("mon_service5");
//		Créez un nouveau service sh de classe SmartHome portant le nom « myKWHome »
//		Service sh = new SmartHome("myKWHome");
//		Créez un nouveau service qs de classe QuantifiedSelf portant le nom « RUNstats »
//		Service qs = new QuantifiedSelf("RUNstats");
		
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
//		t1.subscribe(s1);
//		t1.subscribe(s2);
//		t1.subscribe(s3);
//		t1.subscribe(s4);
//		t1.subscribe(s5);
//		t1.subscribe(sh);
//		t1.subscribe(qs);
//		Abonnez-le à au moins deux services de s1 à s5 (hors QuantifiedSelf et SmartHome).
//		t2.subscribe(s1);
//		t2.subscribe(s2);
//		t2.subscribe(s3);
//		t2.subscribe(s4);
//		t2.subscribe(s5);
		// Appelez la méthode update() de t1.
//		t1.update();
//		 Instanciez un objet k de classe KeyboardInput.
//		KeyboardInput k=new KeyboardInput();
		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
//		k.open();
		f.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
//		p.addService(s1);
//		p.addService(s2);
//		p.addService(s3);
//		p.addService(s4);
//		p.addService(s5);
//		Ajoutez sh à la plateforme p.
//		p.addService(sh);
//		Ajoutez-qs à la plateforme p
//		p.addService(qs);
//		p.addThing(t1);
//		Ajoutez-le à la plateforme p.
//		p.addThing(t2);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		À la place, faîtes simplement un appel à la méthode loadFromDatabase() sur la plateforme p
		p.loadFromDatabase();
		p.run(f);
		
//		p.run(k);
		f.close();
//		s1.close();
//		s2.close();
//		s3.close();
//		s4.close();
//		s5.close();
//		sh.close();
//		qs.close();
		System.out.println("bye.");
	}
	void exo5_1() throws IOException, SQLException {
		System.out.println("Welcome on IoT central platform.");
//		Modifiez la classe Run et instanciez un objet s de classe SocketServer qui écoute sur le port 51291 et qui sera le DataReceiver utilisé par la méthode run() de la plateforme p.
		SocketServer s = new SocketServer(51291);
		
//		Service s1 = new Service("mon_service");
//		Thing t1 = new Thing("1", "f0:de:f1:39:7f:17");
//		Instanciez un objet t2 de classe ThingTempo avec comme adresse MAC "f0:de:f1:39:7f:18", 1 comme id utilisateur et 60 comme délai en secondes.
//		Thing t2 = new ThingTempo("f0:de:f1:39:7f:18", "1",60);
//		s1.writedata(t1);
		
		// Créez quatre nouveaux services s2, s3, s4 et s5.
//		Service s2 = new Service("mon_service2");
//		Service s3 = new Service("mon_service3");
//		Service s4 = new Service("mon_service4");
//		Service s5 = new Service("mon_service5");
//		Créez un nouveau service sh de classe SmartHome portant le nom « myKWHome »
//		Service sh = new SmartHome("myKWHome");
//		Créez un nouveau service qs de classe QuantifiedSelf portant le nom « RUNstats »
//		Service qs = new QuantifiedSelf("RUNstats");
		
		// Abonnez l'objet t1 à ces nouveaux services en utilisant sa méthode
		// subscribe().
//		t1.subscribe(s1);
//		t1.subscribe(s2);
//		t1.subscribe(s3);
//		t1.subscribe(s4);
//		t1.subscribe(s5);
//		t1.subscribe(sh);
//		t1.subscribe(qs);
//		Abonnez-le à au moins deux services de s1 à s5 (hors QuantifiedSelf et SmartHome).
//		t2.subscribe(s1);
//		t2.subscribe(s2);
//		t2.subscribe(s3);
//		t2.subscribe(s4);
//		t2.subscribe(s5);
		// Appelez la méthode update() de t1.
//		t1.update();
//		 Instanciez un objet k de classe KeyboardInput.
//		KeyboardInput k=new KeyboardInput();
		FileReader f = new FileReader("simu.txt");
		// Invoquez sa méthode open().
//		k.open();
//		f.open();
//		Modifiez la classe Run et instanciez un objet s de classe SocketServer qui écoute sur le port 51291 et qui sera le DataReceiver utilisé par la méthode run() de la plateforme p.
		s.open();
		//		Instanciez une plateforme p.
		Platform p = new Platform();
		//		Ajoutez-lui les services s1 à s5 et l'objet connecté t1
//		p.addService(s1);
//		p.addService(s2);
//		p.addService(s3);
//		p.addService(s4);
//		p.addService(s5);
//		Ajoutez sh à la plateforme p.
//		p.addService(sh);
//		Ajoutez-qs à la plateforme p
//		p.addService(qs);
//		p.addThing(t1);
//		Ajoutez-le à la plateforme p.
//		p.addThing(t2);
//		Invoquez l'exécution de la plateforme p avec sa méthode run()
//		À la place, faîtes simplement un appel à la méthode loadFromDatabase() sur la plateforme p
		p.loadFromDatabase();
//		Modifiez la classe Run et instanciez un objet s de classe SocketServer qui écoute sur le port 51291 et qui sera le DataReceiver utilisé par la méthode run() de la plateforme p.
		p.run(s);
		
//		p.run(k);
//		f.close();
//		Modifiez la classe Run et instanciez un objet s de classe SocketServer qui écoute sur le port 51291 et qui sera le DataReceiver utilisé par la méthode run() de la plateforme p.
		s.close();
//		s1.close();
//		s2.close();
//		s3.close();
//		s4.close();
//		s5.close();
//		sh.close();
//		qs.close();
		System.out.println("bye.");	
	}

	public Run() throws IOException, SQLException {
		//Pour chaque exercice un Run est personnalisé!!!!!!!
		exo5_1();
	}

	public static void main(String[] args) throws IOException, SQLException {
		new Run();
	}
}